<?php

/**
 * @file
 *
 * Contains the screen for deleting an index.
 * This includes the actual forms and the submit handler.
 */

/**
 * Confirmation form for deleting an index.
 */
function postgresql_fts_delete_confirm($form, $form_state, $iid) {
  // Set the Index ID to be passed to the submit handler
  $form['iid'] = array('#type' => 'value', '#value' => $iid);

  // Build the textual confirmation page
  $index = _postgresql_fts_index_get_info($iid);
  $message = t('Are you sure?');

  $caption = '<p>'.t('This will delete the index for field <b>%field</b> containing the following indexed columns:',
             array('%field' => $index['field'])).'</p>';
  $caption .= '<ul>';
  foreach ($index['columns'] as $column) {
    $caption .= '<li><b>'.$column.'</b></li>';
  }
  $caption .= '</ul>';
  $caption .= '<ul>';
  $caption .= '<p>'.t('And the following instances:').'</p>';
  foreach ($index['instances'] as $instance) {
    $caption .= '<li><b>'.$instance['entity_type'].'</b></li>';
  }
  $caption .= '</ul>';
  $caption .= '<p>'.t('This action cannot be undone!').'</p>';

  return confirm_form($form, $message, 'admin/config/search/postgresql-fts/', $caption, t("Yes, I'm sure"));
}

/**
 * Submit handler for deleting an index.
 */
function postgresql_fts_delete_confirm_submit($form, &$form_state) {
  _postgresql_fts_index_delete_index($form_state['values']['iid']);
  $form_state['redirect'] = 'admin/config/search/postgresql-fts/';
}