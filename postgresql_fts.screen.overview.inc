<?php

/**
 * @file
 *
 * Contains the main overview screen.
 */

require_once('postgresql_fts.helpers.inc');
require_once('postgresql_fts.helpers.render.inc');

/**
 * Render the main overview screen showing the
 * different indexes present in this installation.
 */
function postgresql_fts_overview($form, &$form_state) {
    $form['introduction'] = array(
    '#type' => 'markup',
    '#markup' => '<h2>Current FTS index(es)</h2>',
  );

  // Retrieve index entries from the database, casting PostgreSQL
  // ARRAY types to JSON, otherwise PHP's PDO driver cannot handle them.
  $header = array(
    array('data' => 'Indexed field', 'class' => array('field')),
    array('data' => 'Column(s)', 'class' => array('columns')),
    array('data' => 'Instance(s)', 'class' => array('instances')),
    array('data' => 'Stats', 'class' => array('stats')),
    array('data' => 'Operations', 'colspan' => 2, 'class' => array('operations')),
  );

  $q = 'SELECT iid, field, fts_table, columns, instances '.
       'FROM {postgresql_fts}';
  $indexes = db_query($q)
    ->fetchAll(PDO::FETCH_ASSOC);

  // Prepare a row for each index.
  $table_rows = array();
  foreach($indexes as $index) {
    $build_action =  array(
          '#type' => 'link',
          '#title' => t('Index remaining items'),
          '#href' => 'admin/config/search/postgresql-fts/'.$index['iid'].'/index',
          '#options' => array('attributes' => array('class' => 'button')),
      );

    $delete_action =  array(
          '#type' => 'link',
          '#title' => t('Delete index'),
          '#href' => 'admin/config/search/postgresql-fts/'.$index['iid'].'/delete',
          '#options' => array('attributes' => array('class' => 'button delete')),
    );

    $columns_list = array(
      '#theme' => 'item_list',
      '#type' => 'ul',
      '#items' => json_decode($index['columns']),
      '#attributes' => array(
        'class' => 'columns',
      ),
    );

    $instances_list = array(
      '#theme' => 'item_list',
      '#type' => 'ul',
      '#items' => _postgresql_fts_render_instance_list(json_decode($index['instances'], TRUE)),
      '#attributes' => array(
        'class' => 'instances',
      ),
    );

    $current = _postgresql_fts_index_count_rows($index['iid']);
    $total = _postgresql_fts_field_count_rows($index['field'], $index['iid']);

    $amount = array(
      '#type' => 'markup',
      '#markup' => t('Indexed !rows of !total',
                      array('!rows' => $current,
                        '!total' => $total)),
    );

    $size = _postgresql_fts_index_get_size($index['iid']);
    $disk_size = array(
      '#type' => 'markup',
      '#markup' => t('Size on disk: !size !unit. Including database indexes.',
                     array('!size' => $size['size'],
                           '!unit' => $size['unit'])),
    );

    $progress = array(
      '#type' => 'markup',
      '#markup' => '<meter value="'.$current.'" min="0" max="'.$total.'"></meter>');

    $stats_list = array(
      '#theme' => 'item_list',
      '#type' => 'ul',
      '#items' => array(
        drupal_render($progress),
        drupal_render($amount),
        drupal_render($disk_size),
       ),
      '#attributes' => array(
        'class' => 'stats',
      ),
    );

    $table_rows[] = array(
      $index['field'].' <small>(Index id: '.$index['iid'].')</small>',
      drupal_render($columns_list),
      drupal_render($instances_list),
      drupal_render($stats_list),
      drupal_render($build_action),
      drupal_render($delete_action),
    );
  }

  // Create the table with all the rows.
  // If we have no indexes, display an empty table.
  $form['indexes'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $table_rows,
    '#empty' => t('No FTS indexes defined.'),
  );

  return $form;
}