<?php

/**
 * @file
 *
 * Contains the screen for deleting an index.
 * This includes the actual forms and the submit handler,
 * as well as the batch preparation and processor.
 */

/**
 * Render the confirmation step before indexing an index.
 */
function postgresql_fts_index_confirm($form, $form_state, $iid) {
  $index = _postgresql_fts_index_get_info($iid);

  // Get the limit, if not yet set give a default of 10.
  $limit = variable_get('postgresql_fts_limit', 10);

  $message = t('Ready to index remaining items for field %field',
             array('%field' => $index['field']));

  $caption = '<p>'.t('Are you sure you want to start indexing remaining items for the index on <b>!field</b>?',
               array('!field' => $index['field'])).'</p>';
  $caption .= '<p>'.t('This will index !limit items per cycle.',
                     array('!limit' => $limit)).'</p>';

  $form['iid'] = array('#type' => 'value',
                       '#value' => $iid);

  return confirm_form($form, $message, 'admin/config/search/postgresql-fts/', $caption, t("Yes, I'm ready"));
}

/**
 * Submit handler for indexing an index.
 */
function postgresql_fts_index_confirm_submit($form, &$form_state) {
  $iid = $form_state['values']['iid'];
  $index = _postgresql_fts_index_get_info($iid);

  // Get the limit, if not yet set give a default of 10.
  $limit = variable_get('postgresql_fts_limit', 10);

  $number_of_items = _postgresql_fts_index_count_remaining_items($iid);

  // If no items to process, return to the overview screen
  // and tell the user about it.
  if($number_of_items == 0) {
    drupal_set_message(t('All items already indexed, nothing left to do.'));
    drupal_goto('admin/config/search/postgresql-fts/');
  }

  $cycles = ceil($number_of_items / $limit);

  $batch = array(
    'finished' => 'build_index_batch_finished',
    'title' => t('Indexing "!field"',
             array('!field' => $index['field'])),
    'init_message' => t('Starting the indexing of !field',
                    array('!field' => $index['field'])),
    'progress_message' => t('Processed @current out of @total (!limit entries per cycle).',
                        array('!limit' => $limit)),
    'error_message' => t('Indexing has encountered an error.'),
    'file' => drupal_get_path('module', 'postgresql_fts') . '/postgresql_fts.screen.index_index.inc',
  );

  for($part = 0; $part < $cycles; $part++){
    $operations[] =  array('build_index_batch', array($part, $iid));
  }
  $batch['operations'] = $operations;

  batch_set($batch);
  batch_process('admin/config/search/postgresql-fts/');
}

/**
 * The actual batch processor.
 *
 * Calculates which chunk needs to be fetched,
 * fetches the data for that chunk, constructs
 * the primary key and field data values and
 * finally inserts the field data.
 */
function build_index_batch($part, $iid) {
  $index = _postgresql_fts_index_get_info($iid);

  // Get the limit, if not yet set give a default of 10.
  $limit = variable_get('postgresql_fts_limit', 10);

  // Set the OFFSET to be used in the query, depending
  // on the given $part variable.
  $offset = 0;
  if($part != 0){
    $offset = ($part * $limit - 1);
  }

  // Get the data for this chunk.
  $data = _postgresql_fts_index_get_remaining_items($iid, $offset, $limit);

  foreach($data as $d) {
    $entity_type = $d['entity_type'];
    $entity_id = $d['entity_id'];

    // Load the full entity.
    $entity = entity_load($entity_type, array($entity_id));

    // Get the desired field data.
    $field_data = field_get_items($entity_type, $entity[$entity_id], $index['field']);

    // Get the deltas.
    $deltas = array_keys($field_data);

    // Get the primary key values (first four elements of our data).
    $pk = array_slice($d, 0, 4);

    // Insert the index entry.
    _postgresql_fts_index_update_entry($iid, $pk, $deltas, $field_data);
  }
}
