<?php

/**
 * Find out to which entities a field is attached.
 *
 * @param $field (string)
 *   The machine name of the field for which to load the instances.
 *
 * @return
 *   An array of the different entities as key and their
 *   base table as value.
 */
function _postgresql_fts_views_field_get_entities($field) {
  // Get information about where the field is used.
  $instances = array();

  foreach (field_read_instances(array('field_name' => $field)) as $instance) {
    if(!array_key_exists($instance['entity_type'], $instances)){
      $instances[$instance['entity_type']] = array();
    }
  }

  foreach ($instances as $instance => $info){
    $entity_info = entity_get_info($instance);
    $instances[$instance] = array('base_table' => $entity_info['base table'],
                                  'entity_id_field' => $entity_info['entity keys']['id']);
  }

  return $instances;
}

function _postgresql_fts_views_field_filter_entities($instances) {
  // Construct the list items.
  foreach ($instances as $instance) {
    $field_instances[] =  $instance['entity_type'];
  }

  return $field_instances;
}