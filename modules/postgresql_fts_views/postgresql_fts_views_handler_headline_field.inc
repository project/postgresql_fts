<?php

/**
 * @file
 *
 * Contains the field that will render the output
 * of PostgreSQL's ts_headline() function making it
 * possible to highlight matching keywords in the
 * resulting texts.
 */

class postgresql_fts_views_handler_headline_field extends views_handler_field {

  /**
   * Inform the system about the various option that we
   * set for this PostgreSQL field handler, appending to
   * the options set by the parent class.
   */
  function option_definition() {
    $options = parent::option_definition();

    $options['expose']['contains']['required'] = array(
      'default' => FALSE,
      'bool' => TRUE,
    );

    // Add the different options so they can be saved afterwards.
    $options['filter'] = array('default' => 0);
    $options['startsel'] = array('default' => '<b>');
    $options['stopsel'] = array('default' => '</b>');
    $options['maxwords'] = array('default' => 10);
    $options['minwords'] = array('default' => 2);
    $options['shortword'] = array('default' => 3);
    $options['highlightall'] = array('default' => 'No');
    $options['maxfragments'] = array('default' => 0);
    $options['fragmentdelimiter'] = array('default' => '...');
    $options['format'] = array('default' => 'full_html');
    $options['delta'] = array('default' => 'any');
    $options['separator'] = array('default' => ',');

    return $options;
  }

  /**
   * Build the specific options needed for this Headline field,
   * appending to the options set by the parent class.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['filter'] = array(
      '#type' => 'select',
      '#title' => t('Highlight filter'),
      '#description' => t('Choose which PostgreSQL FTS filter on this display should be used to create the excerpt. </br> Note that this PostgreSQL FTS filter need not to be indexed as it is merely used to provide us with an (exposed) keyword input.'),
      '#options' => $this->_populate_filter_dropdown(),
      '#default_value' => $this->options['filter'],
     );


    $form['startsel'] = array(
      '#type' => 'textfield',
      '#title' => t('Start markup'),
      '#description' => t('Select which HTML element should precede the highlighted word(s). For example: %bold', array('%bold' => '<b>')),
      '#default_value' => $this->options['startsel']);

    $form['stopsel'] = array(
      '#type' => 'textfield',
      '#title' => t('End markup'),
      '#description' => t('Select which HTML element should append the highlighted word(s). For example: %bold', array('%bold' => '</b>')),
      '#default_value' => $this->options['stopsel']);

    $form['maxwords'] = array(
      '#type' => 'textfield',
      '#title' => t('Maximum words'),
      '#description' => t('Select the maximum of words that should be included surrounding the highlighted word(s), adhering to the "Short word" setting.'),
      '#default_value' => $this->options['maxwords'],
      '#size' => 4,
      '#maxlength' => 4,
      '#states' => array(
        'disabled' => array(
          'input[id="psql_fts_minwords"]' => array('filled' => FALSE))));

    $form['minwords'] = array(
      '#type' => 'textfield',
      '#title' => t('Minimum words'),
      '#description' => t('Select the minimum of words that should be included surrounding the highlighted word(s), adhering to the "Short word" setting.'),
      '#default_value' => $this->options['minwords'],
      '#size' => 4,
      '#maxlength' => 4,
      '#id' => 'psql_fts_minwords');

    $form['shortword'] = array(
      '#type' => 'textfield',
      '#title' => t('Short word'),
      '#description' => t('Select the minimum amount of characters a word surrounding the highlighted word should have before being cut off. The default of 3 would be considered a good boundary for most English words. Setting this to a high value might cut of most words surrounding the highlighted word.'),
      '#default_value' => $this->options['shortword'],
      '#size' => 4,
      '#maxlength' => 4);

    $form['highlightall'] = array(
      '#type' => 'select',
      '#title' => t('Highlight all'),
      '#description' => t('Select if you wish to include all words from the text. Setting this to "Yes" will discard the Maximum/Minimum Words and Short Word settings.'),
      '#default_value' => $this->options['highlightall'],
      '#options' => array(
        0 => t('No'),
        1 => t('Yes')),
      '#states' => array(
        'enabled' => array(
          'input[id="psql_fts_maxfragments"]' => array('filled' => FALSE))),
      '#id' => 'psql_fts_highlightall');

    $form['maxfragments'] = array(
      '#type' => 'textfield',
      '#title' => t('Maximum fragments'),
      '#description' => t('Leaving this value at 0 will give you a normal unfragmented headline. Setting a value greater then 0 will create a headline consisting of that much fragments. A fragment consists out of the matching word which PostgreSQL will try and put in the middle surrounded by words based on the Maximum/Minimum Words and Short Word settings. <b>Warning:</b> Setting this value will discard the Highlight all setting.'),
      '#default_value' => $this->options['maxfragments'],
      '#size' => 4,
      '#maxlength' => 4,
      '#id' => 'psql_fts_maxfragments');

    $form['fragmentdelimiter'] = array(
      '#type' => 'textfield',
      '#title' => t('Fragment delimiter'),
      '#description' => t('Set the charater(s) you wish to use to put in between the different fragments. Defaults to an ellipsis (...).'),
      '#default_value' => $this->options['fragmentdelimiter'],
      '#size' => 4,
      '#maxlength' => 4);

    $form['format'] = array(
      '#type' => 'select',
      '#title' => t('Formatter'),
      '#description' => t('Choose by which filter to render the headline text.'),
      '#options' => $this->_populate_format_dropdown(),
      '#default_value' => $this->options['format'],
     );

    // Set our own version of a multiple result form.
    $form['multiple_result_settings'] = array(
      '#type' => 'fieldset',
      '#title' => t('Multiple result settings'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#weight' => 5,
    );

    $form['delta'] = array(
      '#type' => 'select',
      '#title' => t('Which delta to show'),
      '#description' => t('Choose which specific delta you wish to show if multiple results are found.'),
      '#options' => array(
        'all' => 'All',
        0 => 0,
        1 => 1,
        2 => 2,
        3 => 3,
        4 => 4,
        5 => 5,
        6 => 6,
        7 => 7,
        8 => 8,
        9 => 9),
      '#default_value' => $this->options['delta'],
      '#fieldset' => 'multiple_result_settings',
     );

    $form['separator'] = array(
      '#type' => 'textfield',
      '#title' => t('Separator'),
      '#description' => t('If multiple results are returned, choose which separator to use to stitch them together.'),
      '#default_value' => $this->options['separator'],
      '#fieldset' => 'multiple_result_settings',
    );


    // Remove some default options from the form
    // that don't make sense for this handler.
    unset($form['expose']['multiple']);
    unset($form['expose']['remember']);
    unset($form['expose']['remember_roles']);
  }

  function options_validate(&$form, &$form_state) {
    parent::options_validate($form, $form_state);

    // MinWords cannot be 0, it has to be a positive value if Maximum words is set.
    if($form_state['values']['options']['minwords'] == 0 &&
      $form_state['values']['options']['minwords'] != '') {
      form_error($form['minwords'], t('If setting "Maximum words", "Minimum words" has to be a value greater then 0.'));
    };
    // MinWords cannot be higher than or equal to MaxWords.
    if($form_state['values']['options']['minwords'] >= $form_state['values']['options']['maxwords']) {
      form_error($form['minwords'], t('"Minimum words" has to be less than "Maximum words".'));
    };
  }

  /**
   * Provide extra data for the administration form.
   */
  function admin_summary() {
    $filters = $this->_populate_filter_dropdown();
    foreach($filters as $filter_id => $filter) {
      if($filter_id == $this->options['filter']) {
        return t('Using !filter', array('!filter' => $filter));
        break;
      }
    }
  }

  /**
   * Remove any query setup given by the parent.
   */
  function query() {
    return;
  }

  /**
   * Gather the headlines for each row.
   */
  function pre_render(&$values) {
    // Fetch the ID of the chosen filter.
    $filter_id = $this->options['filter'];

    // Skip the pre_render if $filter_id is not set yet.
    if($filter_id === 0) {
      exit;
    }

    $column = $this->view->postgresql_fts[$filter_id]['column'];
    $fts_table = $this->view->postgresql_fts[$filter_id]['fts_table'];

    // @TODO: Support multiple columns?
    $document = $column;

    // Capture the value of the desired filter.
    $filter_value = $this->view->postgresql_fts[$filter_id]['value'];

    // Figure out which table contains the actual field data.
    $field_table = $this->view->postgresql_fts[$filter_id]['field_table'];

    // See which field is the entity ID field (NID in case of nodes).
    $entity_id_field = $this->view->postgresql_fts[$filter_id]['entity_id_field'];

    // Find which operator the chosen FTS filter in this display has.
    $operator = "plainto_tsquery('".$filter_value."')";

    // Set the different options to be passed to ts_headline().
    $options = array();
    foreach($this->options as $name => $option) {
      if($option != '') {
        switch($name) {
          case 'startsel':
            $options[] = 'StartSel='.$this->options['startsel'];
            break;
          case 'stopsel':
            $options[] = 'StopSel='.$this->options['stopsel'];
            break;
          case 'minwords':
            $options[] = 'MinWords='.$this->options['minwords'];
            break;
          case 'maxwords':
            if($this->options['minwords'] != '') {
              $options[] = 'MaxWords='.$this->options['maxwords'];
            }
            break;
          case 'shortword':
            $options[] = 'ShortWord='.$this->options['shortword'];
            break;
          case 'highlightall':
            $options[] = 'HighlightAll='.($this->options['highlightall'] == 0 ? 'FALSE' : 'TRUE');
            break;
          case 'maxfragments':
            $options[] = 'MaxFragments='.$this->options['maxfragments'];
            break;
          case 'fragmentdelimiter':
            $options[] = 'FragmentDelimiter='.$this->options['fragmentdelimiter'];
            break;
        }
      }
    }

    // Stitch the ts_headline function.
    $ts_headline = "ts_headline(".$document.", ".$operator.", '".implode(', ', $options)."')";

    foreach($values as $id => $value) {
      // Gather the entity ID.
      $entity_id = $value->$entity_id_field;

      // Gather the delta(s) to fetch.
      $deltas = 0;

      // Do the ts_headline() conversion for the given data.
      $q = 'SELECT '.$ts_headline.' AS headline '.
        'FROM {'.$field_table.'} '.
        'WHERE entity_id = :entity_id ';

      $query = db_query($q, array(':entity_id' => $entity_id))
        ->fetchAll(PDO::FETCH_ASSOC);

      if($this->options['delta'] !== 'all') {
        $value->headline = $query[$this->options['delta']]['headline'];
      }
      else {
        foreach($query as $result) {
          $value->headline .= $result['headline'] . $this->options['separator'];
        }
      }
    }
  }

  function render($values) {
    // Simply return the formatted results from the database,
    // pulling it through the desired Drupal format filter.
    return check_markup($values->headline, $this->options['format']);
  }

  /**
   * Tiny helper to prepare the filter listing for
   * use in a Drupal Render Array.
   */
  private function _populate_filter_dropdown() {
    $filters_dropdown = array();
    // Fetch the custom FTS filter data present on the Views object
    // and build a key => value array for Drupal (key being the FTS filter ID).
    foreach($this->view->postgresql_fts as $filter) {
      $filters_dropdown[$filter['filter_id']] = t('Filter for field: !field',
                                                array('!field' => $filter['field']));
    }

    return $filters_dropdown;
  }

  /**
   * Tiny helper to prepare the format listing for
   * use in a Drupal Render Array.
   */
  private function _populate_format_dropdown() {
    $formats_dropdown = array();
    foreach(filter_formats() as $key => $format) {
      $formats_dropdown[$key] = $format->name;
    }

    return $formats_dropdown;
  }
}