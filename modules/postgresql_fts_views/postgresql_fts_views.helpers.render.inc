<?php

/**
 * @file
 *
 * Helper functions to aid in building render arrays and
 * decent screens. Most of the functions are somewhat
 * hacky and ugly.
 */

/**
 * Render a list containing information about the instances given.
 *
 * @param $instances (array)
 *   An array of the instances to render.
 */
function _postgresql_fts_views_render_instance_list($instances) {
  // Construct the list items.
  foreach ($instances as $instance) {
      $field_instances[] = t('!bundle (!entity)',
                           array('!entity' => $instance['entity_type'],
                                 '!bundle' => $instance['bundle']));
    }

  return $field_instances;
}