<?php

class postgresql_fts_views_handler_filter extends views_handler_filter {

  function init(&$view, &$options) {
    parent::init($view, $options);

    // Slap some basic information about this filter
    // onto the Views object for use in other handlers.
    // These options are extended in the query() method of
    // this handler to provide query specific information
    // to other handlers.
    $this->view->postgresql_fts[$options['id']]['filter_id'] = $options['id'];
    $this->view->postgresql_fts[$options['id']]['fts_table'] = $this->definition['data']['fts_table'];
    $this->view->postgresql_fts[$options['id']]['column'] = $this->options['column'];
    $this->view->postgresql_fts[$options['id']]['field'] = $this->definition['data']['field'];
    $this->view->postgresql_fts[$options['id']]['field_table'] = $this->definition['data']['field_table'];
    $this->view->postgresql_fts[$options['id']]['entity_id_field'] = $this->definition['data']['entity_id_field'];
  }

  /**
   * Inform the system about the various option that we
   * set for this PostgreSQL filter handler, appending to
   * the options set by the parent class.
   */
  function option_definition() {
    $options = parent::option_definition();

    $options['expose']['contains']['required'] = array(
      'default' => FALSE,
      'bool' => TRUE,
    );

    // Add the column option so it can be saved afterwards.
    $options['column'] = array('default','default');

    return $options;
  }

  /**
   * Build the specific options needed for this PostgreSQL
   * FTS filter, appending to the options set by the parent class.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['value'] = array(
        '#type' => 'textfield',
        '#title' => t('Search string'),
        '#description' => t('Enter your Full Text search string.'),
        '#size' => 60,
        '#default_value' => $this->value,
      );

    $form['column'] = array(
      '#type' => 'select',
      '#title' => t('Indexed columns'),
      '#description' => t('Select on which indexed columns you want to perform an FTS search.'),
      '#options' => array_flip($this->definition['data']['columns']),
      '#default_value' => $this->options['column'],
    );

    // Remove some default options from the form
    // that don't make sense for this handler.
    unset($form['expose']['multiple']);
    unset($form['expose']['remember']);
    unset($form['expose']['remember_roles']);
  }

  function value_form(&$form, &$form_state) {
    parent::value_form($form, $form_state);

    $form['value'] = array(
        '#type' => 'textfield',
        '#title' => t('Search string'),
        '#description' => t('Enter your Full Text search string.'),
        '#size' => 60,
        '#default_value' => $this->value,
      );
  }

  /**
   * Set the different operators that PostgreSQL
   * FTS system supports.
   */
  function operators() {
    $operators = array(
      'plain' => array(
        'title' => t('Default operator (@@)'),
        'short' => t('Plain'),
        'method' => 'op_plain',
        'values' => 1,
      ),
    );

    return $operators;
  }

  /**
   * Build the strings that represent the different operator
   * options in the operators radio button list rendered
   * by the parent class.
   */
  function operator_options($which = 'title') {
    $options = array();
    foreach ($this->operators() as $id => $info) {
      $options[$id] = $info[$which];
    }

    return $options;
  }


  function operator_values($values = 1) {
    $options = array();
    foreach ($this->operators() as $id => $info) {
      if (isset($info['values']) && $info['values'] == $values) {
        $options[] = $id;
      }
    }

    return $options;
  }

  /**
   * Prepare a string to display in the overview screen,
   * next to the filter.
   */
  function admin_summary() {
    if ($this->is_a_group()) {
      return t('grouped');
    }
    if (!empty($this->options['exposed'])) {
      return t('exposed');
    }

    $options = $this->operator_options('short');
    $output = '';
    if (!empty($options[$this->operator])) {
      $output = check_plain($options[$this->operator]);
    }
    if (in_array($this->operator, $this->operator_values(1))) {
      $output .= ' ' . check_plain($this->value);
    }
    return $output;
  }

  /**
   * Add this filter to our query.
   */
  function query() {
    $this->ensure_my_table();

    // Fetch the index data patched through by the filter handler creator.
    $data = $this->definition['data'];

    // Assign the needed query settings.
    $placeholder = $this->placeholder();
    $entity_base_table = $data['entity_base_table'];
    $entity_base_table_alias = $this->ensure_my_table($entity_base_table);
    $entity_id_field = $data['entity_id_field'];
    $fts_table = $data['fts_table'];
    $fts_column = 'entity_id';
    $fts_table_alias = 'pgfts';

    // Prepare to join the FTS index table to the entity table.
    $join = new views_join;
    $join->left_table = $entity_base_table_alias;
    $join->left_field = $entity_id_field;
    $join->table = $fts_table;
    $join->field = $fts_column;
    $join->type = 'LEFT';

    // Do the join.
    $join_alias = $this->query->add_relationship($fts_table_alias, $join, $entity_base_table);

    // Invoke the correct operator.
    $ops = $this->operators();
    if (!empty($ops[$this->operator]['method'])) {
      $column = $this->options['column'];
      // Perform the actual Full Text filtering by calling
      // the correct operator function which will add
      // a WHERE clause to the mix.
      $this->{$ops[$this->operator]['method']}($join_alias);

      // Continue to slap information onto the Views object.
      // This time add information about the extension just added
      // to the query object for other handlers to use.
      // Note: This extra information is thus *only* available in
      // the query() methods of other handlers.
      $filter_id = $this->options['id'];
      $this->view->postgresql_fts[$filter_id]['alias'] = $join_alias;

      // The value of an exposed filter becomes an array.
      // For now we are only interested in the keywords entered
      // which is the first entry in the array.
      // This might bite us later ...
      if($this->options['exposed'] == 1) {
        $this->view->postgresql_fts[$filter_id]['value'] = $this->value[0];
        $this->view->postgresql_fts[$filter_id]['formula'] = "plainto_tsquery('".$this->value[0]."')";
      }
      else {
        $this->view->postgresql_fts[$filter_id]['value'] = $this->value;
        $this->view->postgresql_fts[$filter_id]['formula'] = "plainto_tsquery('".$this->value."')";
      }
    }
  }

  /**
   * Perform the actual PostgreSQL FTS search based on the default
   * '@@' operator based on the join object (alias) prepare in the query method.
   */
  function op_plain($join_alias) {
    $column = $this->options['column'];
    $this->query->add_where_expression($this->options['group'], "$join_alias.$column @@ plainto_tsquery(:value)", array(':value' => $this->value));
  }
}