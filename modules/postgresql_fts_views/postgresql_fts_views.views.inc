<?php

require_once('postgresql_fts_views.helpers.inc');
require_once('postgresql_fts_views.helpers.render.inc');

function postgresql_fts_views_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'postgresql_fts_views'),
    ),
    'handlers' => array(
      'postgresql_fts_views_handler_filter' => array(
        'parent' => 'views_handler_filter',
      ),
      'postgresql_fts_views_handler_headline_field' => array(
        'parent' => 'views_handler_field',
      ),
      'postgresql_fts_views_handler_ranking_sort' => array(
        'parent' => 'views_handler_sort',
      ),
    ),
  );
}

function postgresql_fts_views_views_data() {
  $data = array();

  // Fetch full information about every index we have.
  $q = 'SELECT field, fts_table, columns, instances '.
       'FROM {postgresql_fts}';
  $indexes = db_query($q)
    ->fetchAll(PDO::FETCH_ASSOC);

  // Tell Views the basic information about the various
  // Full Text index tables present.
  foreach($indexes as $index) {
    // Since we need to tell Views about the relationship
    // between our FTS index tables and to which they join,
    // we need to find the different entities a field belongs to.
    $entities = _postgresql_fts_views_field_get_entities($index['field']);
    foreach($entities as $entity => $entity_info){
      // If the entity we found does not have an index
      // (instances column in our FTS bookkeeping record
      //  does not contain this bundle/entity), skip it.
      $instances = json_decode($index['instances'], TRUE);
      if(!in_array($entity, _postgresql_fts_views_field_filter_entities($instances))) {
        continue;
      }

      $fts_table = $index['fts_table'];
      $field = $index['field'];
      $columns = json_decode($index['columns'], TRUE);
      $entity_base_table = $entity_info['base_table'];
      $entity_id_field = $entity_info['entity_id_field'];

      // Add the table group.
      $data[$fts_table]['table']['group'] = t('Full Text Table');

      // Explain the basic information
      $data[$fts_table]['table']['base'] = array(
        'field' => 'entity_id', // This is the identifier field for the view.
        'title' => t('PostgreSQL FTS index table for !field',
                     array('!field', $field)),
        'help' => t('Table containing the Tsvectors for !columns of !field.',
                    array('!columns' => implode(', ', $columns),
                          '!field' => $field)),
        'weight' => -10,
      );

      // Explain how this table relates to its entity table.
      $data[$fts_table]['table']['join'] = array(
        $entity_base_table => array(
          'left_field' => $entity_id_field,
          'field' => 'entity_id',
        ),
      );

      // Tell Views about our new filter.
      // The first item in the $data array reflects on the table
      // to which this filter joins.
      // Pass all relevant data to this filter handler to use
      // inside the handler and to let this handler pass this
      // information to other handlers.
      $data[$entity_base_table][$fts_table] = array(
        'group' => t('PostgresSQL FTS'),
        'title' => t('for field: !field',
                     array('!field' => $index['field'])),
        'help' => t('Index on column(s): !columns and instance(s): !instances.',
                    array( '!columns' => implode(', ', $columns),
                           '!instances' => implode(', ', _postgresql_fts_views_render_instance_list($instances)))),
        'filter' => array(
          'handler' => 'postgresql_fts_views_handler_filter',
          'data' => array('field' => $field,
                          'fts_table' => $fts_table,
                          'columns' => $columns,
                          'entity_base_table' => $entity_base_table,
                          'entity_id_field' => $entity_id_field,
                          'field_table' => _postgresql_fts_field_get_table_name($field),
          ),
        ),
      );
    }
  }

  // Add the Headline (ts_headline) field handler
  // and pass it data regarding the PostgreSQL filters in use.
  $data['views']['headline'] = array(
    'group' => t('PostgresSQL FTS'),
    'title' => t('Highlight search results.'),
    'help' => t('Field containing excerpts of text that match the keywords and highlights them.</br></br><b>Warning:</b> Due to the nature of the ts_headline() function this field cannot use a FTS (Gist or Gin) index. It is therefor advised to add this field to a small result set or use a pager to chop up larger sets.'),
    'field' => array(
      'handler' => 'postgresql_fts_views_handler_headline_field',
    ),
  );

  // Add the Rank (ts_rank) sort handler
  // and pass it data regarding the PostgreSQL filters in use.
  $data['views']['rank'] = array(
    'group' => t('PostgresSQL FTS'),
    'title' => t('Rank search results.'),
    'help' => t('Rank search results based on a FTS filter and PostgreSQL\'s ts_rank() function.'),
    'sort' => array(
      'handler' => 'postgresql_fts_views_handler_ranking_sort',
    ),
  );

  return $data;
}