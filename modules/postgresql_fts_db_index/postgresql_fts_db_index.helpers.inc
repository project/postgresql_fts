<?php

/**
 * @file
 *
 * Helper functions to aid in managing database indexes.
 */

/**
 * Given an internal relation ID, return the size on disk
 * of the given relation (in this case an index).
 *
 * @param $relid (integer)
 *   The internal relationship ID for which to fetch the size.
 *   These IDs can be found in the pg_class dentition table.
 *
 * @return
 *   An array containing the size converted to
 *   the right unit and the unit itself.
 */
function _postgresql_fts_db_index_get_size($relid) {
  // Load helpers from the main module.
  module_load_include('inc', 'postgresql_fts', 'postgresql_fts.helpers.render');

  $q = 'SELECT pg_relation_size(:relid) AS size';
  $query = db_query($q, array(':relid' => $relid))
    ->fetch(PDO::FETCH_ASSOC);

  return _postgresql_fts_render_size($query['size']);
}

/**
 * Retrieve full index information on all FTS tables in
 * our installation or on a single index if given a
 * relationship id.
 *
 * @param $relid (integer)
 *   The internal relationship ID of the to-be-checked index
 *   that is related one of the FTS tables. If left empty,
 *   all indexes linked to the FTS tables will be returned.
 *
 * @return
 *   An Array keyed by FTS index id containing the FTS index id, the FTS field,
 *   the FTS table, the type of index (Gist or Gin), the FTS columns and
 *   the internal relationship ID (pg_index and pg_class classification).
 */
function _postgresql_fts_db_index_get_indexes($relid = NULL) {
  $relid_query_part = !is_null($relid) ? 'AND c.oid = '.$relid.' ' : '';

  /**
   * Note: The {} in the PostgreSQL "trim" function is a tiny hack to
   *       let Drupal fill in the currently used database prefix.
   *       We need to shave this prefix off of the PostgreSQL table name
   *       in order to make a match with the FTS index table names stored
   *       in the PostgreSQL FTS bookkeeping table (which is stored without
   *       a database prefix).
   */
  $q = 'SELECT c.relname AS index_name, c.oid AS index_relid, '.
              "cc.relname AS fts_table, ".
              'array_to_string(array_agg(a.attname), \',\') AS columns, '.
              'p.iid AS iid, p.field AS field, am.amname AS type '.
              'FROM pg_class c '.
              'LEFT JOIN pg_index i on i.indexrelid = c.oid '.
              'LEFT JOIN pg_class cc on cc.oid = i.indrelid '.
              'LEFT JOIN pg_attribute a on a.attrelid = c.oid '.
              "LEFT JOIN {postgresql_fts} p on p.fts_table = trim(leading '{}' from cc.relname) ".
              'LEFT JOIN pg_am am on am.oid = c.relam '.
              'WHERE c.relkind = :relkind '.
                'AND p.iid NOTNULL '.
                $relid_query_part.
              'GROUP BY p.iid, p.field, c.oid, c.relname, cc.relname, am.amname';
  $query = db_query($q, array(':relkind' => 'i'))
    ->fetchAll(PDO::FETCH_ASSOC);

  $db_indexes = array();
  foreach($query as $db_index) {
    // Convert eh column string into an array.
    $columns = explode(',', $db_index['columns']);
    // Build the lot.
    $db_indexes[$db_index['iid']][] = array('iid' => $db_index['iid'],
                                            'field' => $db_index['field'],
                                            'fts_table' => $db_index['fts_table'],
                                            'type' => $db_index['type'],
                                            'columns' => $columns,
                                            'relid' => $db_index['index_relid'],
                                            'index_name' => $db_index['index_name']);
  }

  return $db_indexes;
}

/**
 * Delete a database index that is related to one of the FTS tables.
 * By design this function only deletes a database index if:
 *
 * - You have permission to access the deletion link (Drupal permissions)
 * - The database index is linked to an FTS table
 * - The database index is linked to the FTS table linked to the given iid
 *
 * In all other cases it will throw an exception.
 *
 * @param $iid (integer)
 *   The ID of the FTS index for which to delete
 *   the database index on its table.
 * @param $relid (integer)
 *   The internal relationship ID of the to-be-deleted
 *   database index.
 */
function _postgresql_fts_db_index_delete($iid, $relid) {
  /**
   * Note: The {} in the PostgreSQL "trim" function is a tiny hack to
   *       let Drupal fill in the currently used database prefix.
   *       We need to shave this prefix off of the PostgreSQL table name
   *       in order to make a match with the FTS index table names stored
   *       in the PostgreSQL FTS bookkeeping table (which is stored without
   *       a database prefix).
   */

  // See if we can fetch the database index based on given data.
  $q = 'SELECT c.relname AS index_name '.
       'FROM pg_class c '.
       'LEFT JOIN pg_index i on i.indexrelid = c.oid '.
       'LEFT JOIN pg_class cc on cc.oid = i.indrelid '.
       'LEFT JOIN pg_attribute a on a.attrelid = c.oid '.
       "LEFT JOIN {postgresql_fts} p on p.fts_table = trim(leading '{}' from cc.relname) ".
       'WHERE c.relkind = :relkind '.
         'AND p.iid = :iid '.
         'AND c.oid = :relid ';
  $query = db_query($q, array(':relkind' => 'i',
                              ':iid' => $iid,
                              ':relid' => $relid))
    ->fetchAll(PDO::FETCH_ASSOC);

  // If no results are returned, we can assume that
  // the given iid and relid did not match, abort.
  if(!$query) {
    throw new Exception('Given iid and relationship ID do not match. Aborting.');
  }

  $index_name = $query[0]['index_name'];

  // If we have a match, continue with the deletion.
  $q = 'DROP INDEX '.$index_name;
  $query = db_query($q);

  watchdog('PostgreSQL FTS',
           t('Deleted database index on FTS index !id'),
           array('!id' => $iid));
}
