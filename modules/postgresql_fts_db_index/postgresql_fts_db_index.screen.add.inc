<?php

/**
 * @file
 *
 * Contains the screen for adding a new database index.
 * This includes the actual forms, the validator
 * and the submit handler.
 */

/**
 * Render the settings form to add a database index
 * and configure which columns you would like to include
 * as well as which type you like to use.
 */
function postgresql_fts_db_index_add($form, &$form_state) {
  // Load helpers from the main module.
  module_load_include('inc', 'postgresql_fts', 'postgresql_fts.helpers');

  // Build a small introduction text.
  $form['introduction'] = array(
    '#type' => 'markup',
    '#markup' => '<h2>Add a database index</h2>',
  );

  $intro = 'Here you can add a new database index on top of you FTS index tables.</br><br/>'.
           '<div class="messages warning"><b>Alpha warning</b>: If your FTS index data set is large (> 100.000 records) '.
           'then database index creation can take a long time and may exceed the '.
           'PHP timout limit of 30 seconds.<br/> Please let me know if this happens to you as I am considering '.
           'pushing these task off to a background worker but currently postponing this '.
           'as this will bring some extra fragile dependencies onto the module.</div>';

  $form['introduction'] = array(
    '#type' => 'markup',
    '#markup' => t($intro),
  );

  $q = 'SELECT iid, field, fts_table, columns, instances '.
       'FROM {postgresql_fts}';
  $indexes = db_query($q)
    ->fetchAll(PDO::FETCH_ASSOC);

  foreach($indexes as $index) {
    $options[$index['iid']] = $index['field'].' (id: '.$index['iid'].')';
  }

  // Add a bogus "Select a field" option to the mix.
  $bogus = array(0 => t('- select a FTS index -'));
  $options = $bogus + $options;

  $description = t('Select on which FTS index (table) you wish to creation a database index.');

  // Build the "fields" dropdown.
  $form['fts_index'] = array(
    '#type' => 'select',
    '#title' => t('Available FTS indexes'),
    '#description' => $description,
    '#options' => $options,
    '#ajax' => array(
      'callback' => 'postgresql_fts_db_index_add_ajax',
      'wrapper' => 'index_settings_wrapper',
      'effect' => 'fade',
    ),
  );

  // Put in the AJAX placeholders for column(s).
  $form['columns'] = array(
      '#type' => 'markup',
      '#markup' => '<div id="index_settings_columns"></div>',
  );
  $form['type'] = array(
      '#type' => 'markup',
      '#markup' => '<div id="index_settings_type"></div>',
  );

  // Build the columns dropdown, preparing it for an AJAX reload.
  if (isset($form_state['values']['fts_index'])) {
    // Gather the full index data.
    $iid = $form_state['values']['fts_index'];
    $index = _postgresql_fts_index_get_info($iid);
    // Fetch the columns, we need to flip the array for we now need
    // the actual column names instead of the abstracted ones.
    $columns = array_flip($index['columns']);

    // Render the column(s) of the chosen field.
    $form['columns'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Available column(s) for FTS index "!field" (Id: !id)',
                    array('!field' => $index['field'],
                          '!id' => $iid)),
      '#description' => t('Select which column(s) you wish to make available for this database index.'),
      '#options' => $columns,
      '#prefix' => '<div id="index_settings_columns">',
      '#suffix' => '</div>',
    );

    // Render the types of index (Giist & Gin).
    $description = 'Choose the type of database index you wish to add.<br/><br/>'.
      '<b>Gin</b>: Use if your FTS index table is read more then written to. '.
      'Gin indexes are slower to update but are generally smaller in disk size then Gist and are faster with reading data.<br/>'.
      '<b>Gist</b>: Use if your FTS index table is written to more then read. '.
      'Gist indexes are slower to read and tend to have a larger disk size then Gin but are faster with updating data.';

    $types = array(0 => '- select an index type -', 'gist' => 'Gist', 'gin' => 'Gin');

    $form['type'] = array(
      '#type' => 'select',
      '#title' => t('Type of database index.'),
      '#description' => t($description),
      '#options' => $types,
      '#prefix' => '<div id="index_settings_type">',
      '#suffix' => '</div>',
    );
  }

  // Build the submit button.
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Add new database index'),
  );

  return $form;
}

/**
 * Ajax callback to refresh the Columns list once chosen a field.
 */
function postgresql_fts_db_index_add_ajax($form, &$form_state) {
  if($form_state['values']['fts_index'] === '0'){
    // Reset the markup if needed.
    $form['columns'] = array(
      '#type' => 'markup',
      '#markup' => '<div id="index_settings_columns"></div>',
    );
    $form['types'] = array(
      '#type' => 'markup',
      '#markup' => '<div id="index_settings_type"></div>',
    );
  }

  // Return two AJAX command to re-render the two pieces of markup.
  return array(
    '#type' => 'ajax',
    '#commands' => array(
      ajax_command_replace('#index_settings_columns', render($form['columns'])),
      ajax_command_replace('#index_settings_type', render($form['type'])),
    )
  );
}

/**
 * Validation handler for the Add Admins Settings form.
 *
 * Validate if a field and columns were chosen.
 */
function postgresql_fts_db_index_add_validate($form, &$form_state) {
  // If no fields selected, throw an error and stop validation.
  if($form_state['values']['fts_index'] === '0'){
    form_set_error('fields', t('Please select a FTS index.'));
    return;
  }

  if(isset($form_state['values']['columns'])){
    // If fields selected but no columns, throw an error.
    $columns = array_filter($form_state['values']['columns']);
    if(empty($columns)){
      // @TODO: Make the columns select list color red...
      form_set_error('columns', t('Please select which column(s) you would like to index.'));
    }
  }

  if(isset($form_state['values']['type'])){
    // If fields selected but no columns, throw an error.
    $type = $form_state['values']['type'];
    if(empty($type)){
      // @TODO: Make the columns select list color red...
      form_set_error('type', t('Please select which type of database index you would like to add.'));
    }
  }
}

/**
 * Submit handler.
 *
 * This will create the database index table for the selected FTS index (table).
 *
 */
function postgresql_fts_db_index_add_submit($form, &$form_state) {
  // Load helpers from the main module.
  module_load_include('inc', 'postgresql_fts', 'postgresql_fts.helpers');

  // Load the full index information.
  $iid = $form_state['values']['fts_index'];
  $index = _postgresql_fts_index_get_info($iid);

  // Create the custom database index name based on the FTS index's
  // table name and the current stamp.
  $index_name = $index['fts_table'].'_'.time().'_idx';

  // Get the columns to index.
  $columns = array();
  foreach ($form_state['values']['columns'] as $column) {
    if($column !== 0) {
      $columns[] = $column;
    }
  }

  // Get the type of database index to be made.
  $type = $form_state['values']['type'];

  // Create the database index itself.
  $transaction = db_transaction();
  try {
    $q = 'CREATE INDEX {'.$index_name.'} '.
         'ON {'.$index['fts_table'].'} '.
         'USING '.$type.'('.implode(', ', $columns).')';
    $query = db_query($q);
  }
  catch (Exception $e) {
    // Something not right? Rollback everything and log.
    $transaction->rollback();
    watchdog_exception('PostgreSQL FTS Exception', $e);
    drupal_set_message(t('Could not add database index, please check the log for more details.'), 'error');
    $form_state['redirect'] = 'admin/config/search/postgresql-fts/db-indexes';
    return;
  }

  // If successful, VACUMM ANALYZE the FTS index table to pickup the new index.

  // Commit the transaction by unlinking the transaction variable.
  // This needs to be done for a VACUUM is not allowed within a transaction.
  unset($transaction);

  // Perform the VACUUM ANALYZE to pick up the new index.
  $q = 'VACUUM ANALYZE {'.$index['fts_table'].'}';
  $query = db_query($q);

  // Let the user know everything went according to plane and redirect.
  watchdog('PostgreSQL FTS',
           t('Added database index (!index) on FTS index !id (type: !type)'),
           array('!index' => $index_name, '!id' => $iid, '!type' => $type));

  drupal_set_message(t('New database index successfully added.'));
  $form_state['redirect'] = 'admin/config/search/postgresql-fts/db-indexes';

  return;
}
