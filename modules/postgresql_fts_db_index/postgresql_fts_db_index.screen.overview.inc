<?php

/**
 * @file
 *
 * Contains the main overview screen.
 */

require_once('postgresql_fts_db_index.helpers.inc');

/**
 * Render the main overview screen showing the
 * different indexes present in this installation.
 */
function postgresql_fts_db_index_overview($form, &$form_state) {
  $form['title'] = array(
    '#type' => 'markup',
    '#markup' => '<h2>Current FTS index(es)</h2>',
  );

  $intro = 'You are in control over the various database indexes on your FTS index tables.</br>'.
           'Database indexes are internally used by your PostgreSQL engine to speed up searches if applicable.</br>'.
           'Because you can query a single FTS index table in various ways, it is possible to setup '.
           'multiple database indexes per FTS index table.<br/><br/>'.
           'Here you can find an overview of the database indexes already setup.<br/><br/>';

  $form['introduction'] = array(
    '#type' => 'markup',
    '#markup' => t($intro),
  );

  // For each index, gather its used database indexes.
  $db_indexes = _postgresql_fts_db_index_get_indexes();

  foreach($db_indexes as $iid => $db_index) {
    // Prepare a row for each index.
    $table_rows = array();

    // Set a title per FTS index.
    $form[$iid]['title'] = array(
      '#type' => 'markup',
      '#markup' => '<h3>'.$db_index[0]['field'].' <small>(Index id '.$iid.' - Table: '.$db_index[0]['fts_table'].')</small></h3>',
    );

    // Now build a table of database indexes per FTS index.
    foreach($db_index as $index){
      // Setup the delete button to carry the internal
      // relationship id of the index (pg_index and pg_class information).
      $delete_action =  array(
        '#type' => 'link',
        '#title' => t('Drop index'),
        '#href' => 'admin/config/search/postgresql-fts/db-indexes/'.$iid.'/'.$index['relid'].'/delete',
        '#options' => array('attributes' => array('class' => 'button delete')),
       );

      $columns_list = array(
        '#theme' => 'item_list',
        '#type' => 'ul',
        '#items' => $index['columns'],
        '#attributes' => array(
          'class' => 'columns',
        ),
      );

      $size = _postgresql_fts_db_index_get_size($index['relid']);
      $disk_size = array(
        '#type' => 'markup',
        '#markup' => t('Size on disk: !size !unit',
                        array('!size' => $size['size'],
                              '!unit' => $size['unit'])),
      );

      $stats_list = array(
        '#theme' => 'item_list',
        '#type' => 'ul',
        '#items' => array(
          drupal_render($disk_size),
         ),
        '#attributes' => array(
          'class' => 'stats',
        ),
      );

      $table_rows[] = array(
        ucfirst($index['type']),
        $index['index_name'],
        drupal_render($columns_list),
        drupal_render($stats_list),
        drupal_render($delete_action),
      );
    }

    // Set the header row of the FTS index table.
    $header = array(
      array('data' => 'Type', 'class' => array('db-index-type')),
      array('data' => 'Name', 'class' => array('db-index-name')),
      array('data' => 'Columns', 'class' => array('db-index-column')),
      array('data' => 'Stats', 'class' => array('db-index-stats')),
      array('data' => 'Operations', 'colspan' => 2, 'class' => array('operations')),
    );

    // Build the actual table for this FTS index.
    $form[$iid]['indexes'] = array(
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $table_rows,
      '#empty' => t('No database indexes defined.'),
    );
  }

  return $form;
}