<?php

/**
 * @file
 *
 * Contains the screen for deleting a database index.
 * This includes the actual forms and the submit handler.
 */

require_once('postgresql_fts_db_index.helpers.inc');

/**
 * Confirmation form for deleting a database index.
 */
function postgresql_fts_db_index_delete_confirm($form, $form_state, $iid, $relid) {
  // Load helpers from the main module.
  module_load_include('inc', 'postgresql_fts', 'postgresql_fts.helpers');

  // Set the Index ID to be passed to the submit handler
  $form['iid'] = array('#type' => 'value', '#value' => $iid);
  // Set the internal database index relid to be passed to the submit handler.
  $form['relid'] = array('#type' => 'value', '#value' => $relid);
  // Gather the full db index information
  $db_index = _postgresql_fts_db_index_get_indexes($relid);
  $db_index = $db_index[$iid][0];

  // Build the textual confirmation page
  $index = _postgresql_fts_index_get_info($iid);
  $message = t('Are you sure?');

  $caption = '<p>'.t('This will drop the <b>%type</b> database index <b>%db_index</b> for field <b>%field</b>.',
             array('%type' => $db_index['type'],
                   '%field' => $index['field'],
                   '%db_index' => $db_index['index_name'])).'</p>';
  $caption .= '<p>'.t('This action cannot be undone!').'</p>';

  return confirm_form($form, $message, 'admin/config/search/postgresql-fts/db-indexes/', $caption, t("Yes, I'm sure"));
}

/**
 * Submit handler for deleting an index.
 */
function postgresql_fts_db_index_delete_confirm_submit($form, &$form_state) {
  _postgresql_fts_db_index_delete($form_state['values']['iid'], $form_state['values']['relid']);
  drupal_set_message(t('Database index successfully dropped.'));
  $form_state['redirect'] = 'admin/config/search/postgresql-fts/db-indexes/';
}