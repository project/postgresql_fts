CONTENTS OF THIS FILE
---------------------

 * In short
 * Why
 * How to use (for end users)
 * How it is done (for end users
 * What it is not
 * What is indexing?
 * For developers

## In Short

Professional full text search for folks running Drupal on PostgreSQL 9.0 or higher complete with full Views integration, all fully SQL based.

## Why

The PostgreSQL RDBMS is capable of doing professional Full Text search, right out of the box. For folks using this database, having to painfully implement Java and Solr on their server in order to be able to perform full text searches within their Drupal installation is rather pointless.

Not only do people bring extra moving parts on their machines when choosing Solr, which requires extra maintenance, resources and overhead, they also miss out on doing Full Text search <strong>without</strong> ever needing to leave the SQL language domain.

Solr, and other full text solutions, use their own query language to plow trough their index records. In Views, for example, this means you cannot simply mix an existing View (which is based on SQL) with your newly installed Solr, as they are both beasts of a different world. You thus need to resort to all kinds of trickery people had to develop to bring features you are used to in Views, into Views created based on Solr.

With PostgreSQL's Full Text Search, being *pure* SQL, you do not have to deliver up on any of such functionalities. Everything Views is capable of, everything any SQL based Views extension is capable of, will still be there, enriched with the power of full text search.

## How to use

1. Enable the module
2. Go to the main overview screen: admin/config/search/postgresql-fts
3. Add an index by clicking on "+ Add a field index"
4. Select which field you like to index
5. Choose which columns you like to index
6. Choose which instances (entity/bundle combinations) you like to index
7. Click "Add new field index" to add this index to your installation
8. Back in the overview screen, click on "Index remaining items" to start indexing the field.

## Self-maintaining

The system is self-maintaining. What this means is that this module, in combination with PostgreSQL, will add, update and delete indexed field data when the actual field data is altered. To minimize performance impact, self-maintenance tasks are put on a queue system. The items on these queues are computed on each cron run at a maximum of 100 items per cron.

## How its done

PostgreSQL Full Text Search takes fields as its base and allows you to index all fields present in your installation. For example: If you have a page node type with a body field attached, you can create an index on this body field, choose which columns of this field you like to index (such as value, summary, format, ...) and choose for which entity/bundles you like this index to be available (whether the entity is a Node, User, Taxonomy, A custom entity, ...).

The submodule PostgreSQL Full Text Search Views, included in this module, brings these indexes right into Views by providing you with filters for each created index.

These filters, exposed to visitors or not, give you the ability to enter search strings and let PostgreSQL fire up its FTS engines and full text search those fields for you.

## What it is not

In theory, all fields are indexable. Even non-text fields such as boolean or date fields. However, this project shall never include the ability to provide mechanisms like full text search data range filters.

Why not? First, it makes little sense from a full text search perspective (its all about text after all) and second, there is no need to for everything is *still* SQL. You could use any Views method to search within a date range (such as using the Date modules capabilities). No need to resort to trickery.

All fields indexed with this module will be cast to text before they are indexed.

## What is indexing?

On a more technical level, indexing in the PostgreSQL world means converting your text strings (text within field columns) to so-called lexemes contained within a tsvector data type.

For each index you wish to create, PostgreSQL will create a new table (to not contaminate any field data tables) and store all its lexemes together with the needed reference data to the original field data table.

The Views integration submodule will provide Views with the needed information to properly join these index tables to their entity table counterparts and take care of querying the lexeme data using tsquery data types.

## For developers

At present, there are only two hooks made available:

- hook_postgresql_fts_add_index_after($index)
- hook_postgresql_fts_delete_index_after($index)

Both hooks fire after adding or deleting a new FTS index respectively and carry with them the index information:

- iid (Index ID)
- field (The actual field's machine name)
- fts_table (The name of the FTS index table)
- columns (Array of indexed field columns)
- instances (Array of indexed field instances)


This module is sponsored and maintained by http://shisaa.jp.
