<?php

/**
 * Render the settings screen for this module.
 */
function postgresql_fts_settings($form, &$form_state) {
  $options = array(10 => 10,
             50 => 50,
             100 => 100,
             200 => 200,
             500 => 500,
             1000 => 1000);
  $form['limit'] = array(
    '#type' => 'select',
    '#options' => $options,
    '#title' => t('Batch limit'),
    '#description' => t('Set the limit of items to process per batch cycle.'),
    '#default_value' => variable_get('postgresql_fts_limit', 10),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save settings'),
  );

  return $form;
}

function postgresql_fts_settings_submit($form, &$form_state) {
  variable_set('postgresql_fts_limit', $form_state['values']['limit']);
}