<?php

/**
 * @file
 *
 * Helper functions to aid in building render arrays and
 * decent screens. Most of the functions are somewhat
 * hacky and ugly.
 */

/**
 * Render a list containing information about the instances given.
 *
 * @param $instances (array)
 *   An array of the instances to render.
 */
function _postgresql_fts_render_instance_list($instances) {
  // Construct the list items.
  foreach ($instances as $instance) {
      $field_instances[] = t('Used in <i>!bundle</i> - entity type <i>!entity</i>',
                           array('!entity' => $instance['entity_type'],
                                 '!bundle' => $instance['bundle']));
    }

  return $field_instances;
}

/**
 * Render a list containing information about the instances given.
 * Used for rendering error messages.
 *
 * @param $instances (array)
 *   An array of the instances to render.
 */
function _postgresql_fts_render_instance_list_error($instances) {
  // Construct the list items.
  foreach ($instances as $instance) {
      $field_instances[] = t('!bundle',
                           array('!bundle' => $instance['bundle']));
    }

  return $field_instances;
}

/**
 * Render a size amount in kb, mb or gb.
 *
 * @param $size (integer)
 *   Size in bytes.
 * @param $unit (string)
 *   Unit to express the file size. Possible values:
 *   'kb', 'mb', 'gb', 'auto'.
 *
 * @return
 *   An array containing the size converted to
 *   the right unit and the unit itself.
 */
function _postgresql_fts_render_size($size, $unit = 'auto') {
  // Check if the given unit is valid.
  if(!in_array(strtolower($unit), array('kb', 'mb', 'gb', 'auto'))) {
    throw new Exception('Illegal unit choice, use only "kb", "mb", "gb" or "auto"');
  }

  // If set to auto, set the unit to either kb, mb or gb
  // depending on how many digits we get back.
  if($unit === 'auto') {
    $length = strlen((string) $size);
    if($length <= 5) {
      $unit = 'kb';
    }
    else if ($length > 5 && $length <= 9) {
      $unit = 'mb';
    }
    else {
      $unit = 'gb';
    }
  }

  // Do the math.
  // Not exact sience, but good enough for stats purposes.
  switch($unit){
    case 'kb':
      return array('size' => round($size / 1024, 2), 'unit' => 'Kb');
      break;
    case 'mb':
      return array('size' => round($size / 1024 / 1024, 2), 'unit' => 'Mb');
      break;
    case 'gb':
      return array('size' => round($size / 1024 / 1024 / 1024, 2), 'unit' => 'Gb');
      break;
  }
}