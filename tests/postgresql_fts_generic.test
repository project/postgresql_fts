<?php

/**
 * @file
 * Web testcases for generic behavior.
 */

require_once 'postgresql_fts_test_case.php';
require_once __DIR__ .'/../postgresql_fts.helpers.inc';

/**
 * @class
 *
 * This suite tests the generic forward behavior (using nodes and fields)
 * and contains the regression tests for known and reported bugs.
 */
class PostgresqlFtsGeneric extends PostgresqlFtsTestCase {
  static public function getInfo() {
    return array(
      'name' => 'PostgreSQL FTS Tests - Generic workflow',
      'description' => 'Tests for the generic workflow.',
      'group' => 'PostgreSQL FTS',
    );
  }

  // Generic tests below.

  /**
   * Test if we can reach the overview screen and it
   * is intialy empty.
   */
  public function testEmptyOverviewScreen() {
    // Login as Admin.
    $user = $this->drupalCreateUser(array('administer postgresql fts'));
    $this->drupalLogin($user);

    // Navigate to the overview screen.
    $this->drupalGet('/admin/config/search/postgresql-fts/');

    // See if the list is empty.
    $this->assertRaw('No FTS indexes defined.', 'See if the FTS index list is initially empty.');

    // See if the correct add link is present.
    $this->assertRaw('Add a FTS index', 'See if the "Add a FTS index" link is there.');
  }

  /**
   * Does the Add FTS Index screen work as excepted?
   */
  public function testDefaultAddIndexScreen() {
    // Login as Admin.
    $user = $this->drupalCreateUser(array('administer postgresql fts'));
    $this->drupalLogin($user);

    // Navigate to the overview screen.
    $this->drupalGet('/admin/config/search/postgresql-fts/add');

    // See if the correct add link is present.
    $this->assertText('Add a FTS index', 'See if the "Add a FTS index" title is there.');

    // See if the "Available fields" drop down is present.
    $this->assertFieldById('edit-fields', '', 'The edit-fields exists on the page');

    // See if the other fields are still hidden at this point.
    $this->assertNoFieldById('edit-columns-value', '', 'The edit-columns is not on the page (value select)');
    $this->assertNoFieldById('edit-instances-nodepage', '', 'The edit-instances is not on the page (page select)');

    $this->drupalPostAJAX(NULL, array('fields' => 'body'), 'fields');

    // To be able to test AJAX event build with "ajax_command_replace" returns,
    // a tiny patch has to be applied to core since SimpleTest in Drupal has
    // a known bug which prevents it from testing these situations.
    // Open bug in Drupal core: https://www.drupal.org/node/2066101.
    // If you do not have this patch applied, the below assertions will fail.

    // See if the column and instance checkboxes are now present.
    $this->assertFieldById('edit-columns-value', '', 'The edit-columns is on the page (value select)');
    $this->assertFieldById('edit-instances-nodepage', '', 'The edit-instances is on the page (page select)');

    // Select the "value" column and the "page" instance and submit the form.
    $edit =  array(
      'fields' => 'body',
      'columns[value]' => 1,
      'instances[node|page]' => 1);
    $this->drupalPost(NULL, $edit, t('Add new FTS index'));

    // We should return to the main overview with a success message.
    $this->assertText('New FTS index successfully added.', 'Success message is printed.');

    // Check if an index with the correct columns and instances was created.
    $this->assertText('body_value', 'Column name is present in overview table.');
    $this->assertText('Used in page - entity type node', 'Instance name is present in overview table.');

    // We have to manually clean up the index.
    _postgresql_fts_index_delete_index(1);
  }


  /**
   * When we add a new field to our "page" content type, does the module see it?
   */
  public function testNewField() {
    // Login as Admin.
    $user = $this->drupalCreateUser(array('administer postgresql fts'));
    $this->drupalLogin($user);

    // Create a new text field.
    $field_data = array(
      'field_name' => 'field_page_test',
      'type' => 'text');
    $field = field_create_field($field_data);

    // Create the instance on the bundle.
    $instance = array(
        'field_name' => 'field_page_test',
        'entity_type' => 'node',
        'label' => 'A Test Field',
        'bundle' => 'page',
        'widget' => array(
            'type' => 'textfield',
        ),
    );
    field_create_instance($instance);

    // Navigate to the index add screen.
    $this->drupalGet('/admin/config/search/postgresql-fts/add');

    // Check if the new Test text field is present.
    $this->assertRaw('<option value="field_page_test">field_page_test</option>', 'The new Test field is present.');
  }

  /**
   * Can we delete a index?
   */
  public function testDeleteIndex() {
    // Login as Admin.
    $user = $this->drupalCreateUser(array('administer postgresql fts'));
    $this->drupalLogin($user);

    // Navigate to the overview screen.
    $this->drupalGet('/admin/config/search/postgresql-fts/add');

    // Choose the body field.
    $this->drupalPostAJAX(NULL, array('fields' => 'body'), 'fields');

    // To be able to test AJAX event build with "ajax_command_replace" returns,
    // a tiny patch has to be applied to core since SimpleTest in Drupal has
    // a known bug which prevents it from testing these situations.
    // Open bug in Drupal core: https://www.drupal.org/node/2066101.
    // If you do not have this patch applied, the below assertions will fail.

    // Select the "value" column and the "page" instance and submit the form.
    $edit =  array(
      'fields' => 'body',
      'columns[value]' => 1,
      'instances[node|page]' => 1);
    $this->drupalPost(NULL, $edit, t('Add new FTS index'));

    // First let us see the new index is there.
    $this->assertText('body_value', 'Column name is present in overview table.');
    $this->assertText('Used in page - entity type node', 'Instance name is present in overview table.');

    // Now let us try to delete it.
    $this->drupalPost('/admin/config/search/postgresql-fts/1/delete', array(), t('Yes, I\'m sure'));
    $this->assertNoText('body_value', 'Column name is not present anymore in overview table.');
    $this->assertNoText('Used in page - entity type node', 'Instance name is not present anymore in overview table.');
  }

  /**
  * Can we index a set of 5 nodes?
  */
  public function testIndexing() {
    // Login as Admin.
    $user = $this->drupalCreateUser(array('administer postgresql fts'));
    $this->drupalLogin($user);

    $nids = array();

    include 'postgresql_fts_content.php';

    // Create 5 'page' type nodes.
    $nids[] = $this->create_node($postgresql_fts_test_titles[0], $postgresql_fts_test_bodies[0], 'page');
    $nids[] = $this->create_node($postgresql_fts_test_titles[1], $postgresql_fts_test_bodies[1], 'page');
    $nids[] = $this->create_node($postgresql_fts_test_titles[2], $postgresql_fts_test_bodies[2], 'page');
    $nids[] = $this->create_node($postgresql_fts_test_titles[2], $postgresql_fts_test_bodies[3], 'page');
    $nids[] = $this->create_node($postgresql_fts_test_titles[2], $postgresql_fts_test_bodies[4], 'page');


    // Add a new index on 'page' and the 'body' field.
    $this->drupalGet('/admin/config/search/postgresql-fts/add');
    $this->drupalPostAJAX(NULL, array('fields' => 'body'), 'fields');
    $edit =  array(
      'fields' => 'body',
      'columns[value]' => 1,
      'instances[node|page]' => 1);
    $this->drupalPost(NULL, $edit, t('Add new FTS index'));

    // Check if we find the 5 nodes from above, with 0 indexed.
    $this->assertText('Indexed 0 of 5', '5 nodes found for this index, 0 indexed.');

    // Now index these 5 nodes.
    $this->drupalPost('/admin/config/search/postgresql-fts/1/index', array(), t('Yes, I\'m ready'));

    // Check if we find the 5 nodes from above, with 5 indexed.
    $this->assertText('Indexed 5 of 5', '5 nodes found for this index, 5 indexed.');

    // Clean up the index.
    _postgresql_fts_index_delete_index(1);

    // Clean up the nodes.
    $this->remove_nodes($nids);
  }

  /**
   * Do we detect new nodes added?
   */
  public function testNewNodesAdded() {
    // Login as Admin.
    $user = $this->drupalCreateUser(array('administer postgresql fts'));
    $this->drupalLogin($user);

    $nids = array();

    include 'postgresql_fts_content.php';

    // Create 2 'page' type nodes.
    $nids[] = $this->create_node($postgresql_fts_test_titles[0], $postgresql_fts_test_bodies[0], 'page');
    $nids[] = $this->create_node($postgresql_fts_test_titles[1], $postgresql_fts_test_bodies[1], 'page');

    // Add a new index on 'page' and the 'body' field.
    $this->drupalGet('/admin/config/search/postgresql-fts/add');
    $this->drupalPostAJAX(NULL, array('fields' => 'body'), 'fields');
    $edit =  array(
      'fields' => 'body',
      'columns[value]' => 1,
      'instances[node|page]' => 1);
    $this->drupalPost(NULL, $edit, t('Add new FTS index'));

    // Check if we find the 2 nodes from above.
    $this->assertText('Indexed 0 of 2', '2 nodes found for this index.');

    // Add three new nodes
    $nids[] = $this->create_node($postgresql_fts_test_titles[2], $postgresql_fts_test_bodies[2], 'page');
    $nids[] = $this->create_node($postgresql_fts_test_titles[2], $postgresql_fts_test_bodies[3], 'page');
    $nids[] = $this->create_node($postgresql_fts_test_titles[2], $postgresql_fts_test_bodies[4], 'page');

    // Check if we find the 3 added nodes from above (5 in total).
    $this->drupalGet('/admin/config/search/postgresql-fts');
    $this->assertText('Indexed 0 of 5', '5 nodes found for this index.');

    // Add one new node, but of a different type.
    $nids[] = $this->create_node($postgresql_fts_test_titles[2], $postgresql_fts_test_bodies[0], 'article');

    // Check that we still only have 5 nodes, as "article" is not in this index.
    $this->drupalGet('/admin/config/search/postgresql-fts');
    $this->assertText('Indexed 0 of 5', 'Article node ignored for this index.');

    // Clean up the index.
    _postgresql_fts_index_delete_index(1);

    // Clean up the nodes.
    $this->remove_nodes($nids);
  }

  /**
   * Do the Add a FTS index validation errors trigger?
   */
  public function testAddFTSIndexValidation() {
    // Login as Admin.
    $user = $this->drupalCreateUser(array('administer postgresql fts'));
    $this->drupalLogin($user);

    // Add a new index on 'page' and the 'body' field.
    $this->drupalGet('/admin/config/search/postgresql-fts/add');

    // Try to submit without choosing a field.
    $this->drupalPost(NULL, array(), t('Add new FTS index'));

    // This should trigger a validation error.
    $this->assertText('Please select a field.', 'Field validation error triggered.');

    // Now select a field.
    $this->drupalPostAJAX(NULL, array('fields' => 'body'), 'fields');

    // And try to submit without choosing a column or instance.
    $edit =  array(
      'fields' => 'body');
    $this->drupalPost(NULL, $edit, t('Add new FTS index'));

    // This should trigger both column and instance validation errors.
    $this->assertText('Please select which column(s) you would like to index', 'Column validation error triggered.');
    $this->assertText('Please select which instance(s) you would like to index', 'Instance validation error triggered.');

    // Select only a column but leave instance unchecked.
    $this->drupalGet('/admin/config/search/postgresql-fts/add');
    $this->drupalPostAJAX(NULL, array('fields' => 'body'), 'fields');
    $edit =  array(
      'fields' => 'body',
      'columns[value]' => 1);
    $this->drupalPost(NULL, $edit, t('Add new FTS index'));

    // This should trigger a instance validation error.
    $this->assertText('Please select which instance(s) you would like to index', 'Instance validation error triggered.');

    // Now the reverse, select only an instance but leave column unchecked.
    $this->drupalGet('/admin/config/search/postgresql-fts/add');
    $this->drupalPostAJAX(NULL, array('fields' => 'body'), 'fields');
    $edit =  array(
      'fields' => 'body',
      'instances[node|page]' => 1);
    $this->drupalPost(NULL, $edit, t('Add new FTS index'));

    // This should trigger a column validation error.
    $this->assertText('Please select which column(s) you would like to index', 'Column validation error triggered.');
  }
}