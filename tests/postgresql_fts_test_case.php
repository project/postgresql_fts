<?php

/**
 * @file
 * Unit and Web testcases.
 */

/**
 * @class
 *
 * The actual testsuite which implements the DrupalWebTestcase.
 * This class is only used to add extra assertion methods that
 * are not present in the default DrupalWebTestCase.
 */
class PostgresqlFtsTestCase extends DrupalWebTestCase {
  static public function getInfo () {
    return array(
      'name' => 'PostgreSQL FTS Tests',
      'description' => 'PostgreSQL FTS DrupalWebTestCase implementation.',
      'group' => 'PostgreSQL FTS',
    );
  }

  /**
   * Setup
   *
   * Load the necessary modules.
   */
  public function setUp ($modules = array()) {
    parent::setUp(array('postgresql_fts'));
  }

  /**
   * Assert if a checkbox is checked or not.
   */
  public function assertCheckbox($id, $state) {
    $state = $state ? 'checked' : $state;
    $elements = $this->xpath('//input[@type="checkbox" and @id=:id]', array(':id' => $id));
    $this->assertEqual($elements[0]['checked'],
                       $state, t('Checkbox @id is @state.',
                       array('@id' => $id, '@state' => ($state ? 'checked' : 'unchecked'))),
                       t('Browser'));
  }

  /**
   * Assert if a Textbox is empty or contains a certain value.
   */
  public function assertTextbox($id, $value) {
    $elements = $this->xpath('//input[@type="text" and @id=:id]', array(':id' => $id));
    $this->assertEqual($elements[0]['value'],
                       $value,
                       t('Textbox @id @value.', array('@id' => $id, '@value' => ($value == '') ? 'is empty' : 'contains "'.$value.'"')),
                       t('Browser'));
  }

  /**
   * Create a given amount of nodes (actual, maximum 26, then we run out of letters...).
   */
  public function create_node($title, $body, $type) {
    $node = new stdClass();

    $node->title = $title;
    $node->type = $type;
    node_object_prepare($node);
    $node->language = LANGUAGE_NONE;
    $node->body['und'][0]['value'] = $body;
    $node->status = 1;
    $node->promote = 0;

    $node = node_submit($node);
    node_save($node);

    return $node->nid;
  }

  /**
   * Delete the previously created nodes.
   */
  public function remove_nodes($nids) {
    foreach ($nids as $nid) {
      node_delete($nid);
    }
  }
}
