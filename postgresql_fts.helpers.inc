<?php

/**
 * @file
 *
 * Helper functions to aid in managing FTS indexes and
 * gathering information from fields and their data.
 */

/**
 * Dispatches the queued items to the correct helper function.
 *
 * @param $item (array)
 *   The data accompanying the item on the queue. This is an array
 *   keyed with 'action', 'iid', 'field_name', 'entity' and 'entity_type'.
 *
 * @return
 *   Nothing.
 */
function _postgresql_fts_queue_dispatcher($item) {
  $action = $item[0];
  // Delete the first "action" element.
  array_shift($item);

  if($action === 'delete_index') {
    $data = $item;
  } else {
    $data = _postgresql_fts_index_prepare_data($action, $item);
  }

  switch($action){
    // Dispatch to the correct function.
    case 'insert':
      call_user_func_array('_postgresql_fts_index_insert_entry', $data);
      break;
    case 'update':
      call_user_func_array('_postgresql_fts_index_update_entry', $data);
      break;
    case 'delete':
      call_user_func_array('_postgresql_fts_index_delete_entry', $data);
      break;
    case 'index-delete':
      call_user_func_array('_postgresql_fts_index_delete', $data);
      break;
  }

  return;
}

/**
 * Prepare the necessary data to provide the different
 * index helper function with.
 *
 * @param $action (string)
 *   Which action to prepare data for. Can be 'insert', 'update'
 *   'delete' or 'delete_index'.
 * @param $item (array)
 *   The data accompanying the item on the queue. This array is
 *   composed of the index ID, the field machine name, the full entity
 *   and the entity type.
 */
function _postgresql_fts_index_prepare_data($action, $item) {
  // Extract the iid, field name, entity and entity type from the item data.
  list($iid, $field, $entity, $entity_type) = $item;
  // Extract entity type agnostic info (id, latest revision and bundle).
  list($id, $revision, $bundle) = entity_extract_ids($entity_type, $entity);

  // Compose the primary key.
  $pk = array('entity_type' => $entity_type,
              'entity_id' => $id,
              'deleted' => 0,
              'language' => 'und');
  // Fetch the field data.
  $field_data = field_get_items($entity_type, $entity, $field);

  // Extract the deltas from the field data.
  $deltas = array_keys($field_data);
  // If $deltas is empty, set it to zero.
  // Core fields such as "title" have no delta
  // as they are not properly routed via the Field API.
  if(!$deltas) {
    $deltas = array(0 => 0);
  }

  switch($action){
    case 'insert':
    case 'update':
      return array($iid, $pk, $deltas, $field_data);
      break;
    case 'delete':
      return array($iid, $pk, $deltas);
      break;
  }
}

/**
 * Delete a single entry from an index.
 *
 * @param $iid (integer)
 *   The index ID for which to delete the entry.
 * @param $pk (array)
 *   An array containing the primary key column data
 *   keyed by column name.
 * @param $delta (array)
 *   An array containing the different field deltas.
 *
 * @return
 *   Nothing.
 */
function _postgresql_fts_index_delete_entry($iid, $pk, $deltas){
  // Check if all primary key keys are present
  _postgresql_fts_index_check_pk($pk);

  // Fetch the table name of the index.
  $info = _postgresql_fts_index_get_info($iid);
  $table = $info['fts_table'];

  // Perform the deletion query
  $q = 'DELETE FROM {'.$table.'} '.
       'WHERE entity_type = :entity_type '.
             'AND entity_id = :entity_id '.
             'AND deleted = :deleted '.
             'AND delta IN (:deltas) '.
             'AND language = :language';
  db_query($q, array(':entity_type' => $pk['entity_type'],
                     ':entity_id' => $pk['entity_id'],
                     ':deleted' => $pk['deleted'],
                     ':deltas' => $deltas,
                     ':language' => $pk['language']));

  //@TODO: Maybe capture a PDO exception as well.

  return;
}

/**
 * Insert one or more new entries into an index.
 *
 * @param $iid (integer)
 *   The index ID for which to delete the entry.
 * @param $pk (array)
 *   An array containing the primary key column data
 *   keyed by column name.
 * @param $delta (array)
 *   An array containing the different field deltas.
 * @param $field_data (array)
 *   An array containing the field data keyed by ?????.
 *
 * @return
 *   Nothing.
 */
function _postgresql_fts_index_insert_entry($iid, $pk, $deltas, $field_data) {
  // Check if all primary key keys are present
  _postgresql_fts_index_check_pk($pk);

  // Fetch the table name of the index.
  $info = _postgresql_fts_index_get_info($iid);
  $table = $info['fts_table'];

  // Fetch the columns for this index.
  $columns = $info['columns'];

  // Loop over each delta and create a single INSERT statement.
  $insert = array();
  $table_data = array();
  foreach($deltas as $delta) {
    // Add or overwrite the primary key delta value for this iteration.
    $pk['delta'] = $delta;
    // Clear out any non-indexed columns from our field data.
    $data = array_intersect_key($field_data[$delta], $columns);
    // If the value of the given column is empty, remove it from
    // the data and column arrays so it does not get processed in the query.
    foreach($data as $key => &$d) {
      if($d == '') {
        unset($data[$key]);
        unset($columns[$key]);
      }
    }

    // Prepare the column statement
    $table_columns = '('.implode(', ', array_keys($pk)).', '.implode(', ', $columns).')';

    // Prepare the amount of wildcard (?) placeholders to be bound later on.
    $placeholders = array();
    for($i = 0; $i < (count($pk) + count($data)); $i++) {
      if($i < count($pk)) {
        // Set simple placeholders for the primary keys.
        $placeholders[] = '?';
      }
      else{
        // After the primary keys, assume all needs to be converted to Tsvectors.
        $placeholders[] = 'to_tsvector(?)';
      }
    }
    $table_placeholders[] = '('.implode(', ', $placeholders).')';

    // Prepare the actual data to be bound to the placeholders.
    $table_data = array_merge($table_data, array_values($pk));
    $table_data = array_merge($table_data, array_values($data));
  }

  // Create the INSERT query, binding the values to the placeholders
  // and executing it.
  $q = 'INSERT INTO {'.$table.'} '.
       $table_columns.' '.
       'VALUES '.implode(', ', $table_placeholders);
  $query = db_query($q, $table_data);

  return;
}

/**
 * Update one or more existing index entries.
 *
 * This is basically a wrapper around the index delete
 * and index insert helpers.
 *
 * @param $idd (integer)
 *   The index ID for which to update the entry.
 * @param $pk (array)
 *   An array containing the primary key column data
 *   keyed by column name.
 * @param $field_data (array)
 *   An array containing the field data keyed by ?????.
 *
 * @return
 *   Nothing.
 */
function _postgresql_fts_index_update_entry($iid, $pk, $deltas, $field_data) {
  // Check if all primary key keys are present (except for 'delta').
  _postgresql_fts_index_check_pk($pk);
  // Delete all entries.
  _postgresql_fts_index_delete_entry($iid, $pk, $deltas);
  // Re-insert with their new data.
  _postgresql_fts_index_insert_entry($iid, $pk, $deltas, $field_data);

  return;
}

/**
 * Check if the Field API primary key columns are present
 * in the given array. These keys are always: 'entity_type',
 * 'entity_id', 'deleted' and 'language'. Officially, 'delta'
 * is also part of the PK, but we provide it separately for
 * ease of working with multiple value fields.
 *
 * @param $pk (array)
 *   The array which should contain all the needed keys
 *
 * @return
 *   Throws an exception if not all keys are present,
 *   otherwise returns TRUE.
 */
function _postgresql_fts_index_check_pk($pk) {
  // Check if all primary key keys are present
  $pk_keys = array('entity_type', 'entity_id', 'deleted', 'language');

  if(count(array_intersect_key(array_flip($pk_keys), $pk)) !== count($pk_keys)){
    throw new Exception('Not all primary key columns present');
  }

  // @TODO: Also check all values are not NULL.

  return TRUE;
}

/**
 * Retrieve all the info regarding a given index.
 *
 * @param $iid (integer)
 *   The ID of the index for which to retrieve the table name.
 *
 * @return
 *   An array containing the index information.
 */
function _postgresql_fts_index_get_info($iid) {
  $q = 'SELECT field, fts_table, columns, instances '.
       'FROM {postgresql_fts} '.
       'WHERE iid = :iid';
  $query = db_query($q, array(':iid' => $iid))
    ->fetch(PDO::FETCH_ASSOC);

  $info['field'] = $query['field'];
  $info['fts_table'] = $query['fts_table'];
  $info['columns'] = json_decode($query['columns'], TRUE);
  $info['instances'] = json_decode($query['instances'], TRUE);

  return $info;
}

/**
 * Check if a FTS table with the combination of columns
 * and instances already exists.
 *
 * @param $field (string)
 *   The machine name of the field to check.
 * @param $columns (array)
 *   Array of column names to check for.
 * @param $instances (array)
 *   Array of instance names to check for.
 *
 * @return
 *   TRUE is a combination is found, FALSE otherwise.
 */
function _postgresql_fts_index_check_combination($field, $columns, $instances) {
  $q = 'SELECT fts_table '.
       'FROM {postgresql_fts} '.
       'WHERE field = :field '.
       'AND columns = :columns '.
       'AND instances = :instances';
  $query = db_query($q, array(
             ':field' => $field,
             ':columns' => json_encode($columns),
             ':instances' => json_encode($instances)));

  if($query->rowCount() === 0) {
    return FALSE;
  }

  return TRUE;
}

/**
 * Get the amount of rows in an index.
 *
 * @param $iid (integer)
 *   The ID of the index for which to count rows.
 *
 * @return
 *   The number of rows in the index.
 */
function _postgresql_fts_index_count_rows($iid) {
  $info = _postgresql_fts_index_get_info($iid);
  $table = $info['fts_table'];

  $q = 'SELECT count(entity_id) '.
       'FROM {'.$table.'}';
  $query = db_query($q)
    ->fetch(PDO::FETCH_ASSOC);

  return $query['count'];
}

/**
 * Delete an index entry from the FTS bookkeeping table
 * and its index table.
 *
 * @param $iid (integer)
 *   The ID of the index entry to be deleted.
 *
 * @return
 *   Nothing.
 */
function _postgresql_fts_index_delete_index($iid) {
  // Fetch the table name of the index.
  $index = _postgresql_fts_index_get_info($iid);
  $table = $index['fts_table'];

  // Delete the bookkeeping entry.
  $q = 'DELETE FROM {postgresql_fts} '.
       'WHERE iid = :iid';
  db_query($q, array(':iid' => $iid));

  // Delete the index table itself.
  $q = 'DROP TABLE {'.$table.'}';
  db_query($q);

  watchdog('PostgreSQL FTS',
           t('Deleted FTS index (id: !id) on field !field'),
           array('!id' => $iid, '!field' => $index['field']));

  drupal_set_message(t('FTS index successfully deleted.'));

  // Give other modules a chance to act after deleting an index.
  $index = array('field' => $index['field'],
                 'fts_table' => $index['fts_table'],
                 'columns' => $index['columns'],
                 'instances' => $index['instances']);
  module_invoke_all('postgresql_fts_delete_index_after', $index);

  return;
}

/**
 * Add an index entry to the FTS bookkeeping table.
 *
 * @param $field (string)
 *   The machine name of the field.
 * @param $fts_table (string)
 *   The name of the FTS table in the database.
 * @param $columns (array)
 *   The different columns that this index contains.
 * @param $instances (array)
 *   The different instances that this index contains.
 *
 * @return
 *   Nothing.
 */
function _postgresql_fts_index_add_index($field, $fts_table, $columns, $instances) {
  $q = 'INSERT INTO {postgresql_fts} '.
       '(field, fts_table, columns, instances, created) '.
       'VALUES (:field, :fts_table, :columns, :instances, :created) '.
       'RETURNING iid';
  try{
    $query = db_query($q, array(
               ':field' => $field,
               ':fts_table' => $fts_table,
               ':columns' => json_encode($columns),
               ':instances' => json_encode($instances),
               ':created' => REQUEST_TIME));
  }
  catch(Exception $e){
    watchdog_exception('PostgreSQL FTS Exception', $e);
  }

  // Fetch the index id via the RETURNING clause
  // of the above query.
  $iid = $query->fetch();
  $iid = $iid->iid;

  watchdog('PostgreSQL FTS',
           t('Added new index (id: !id) on field !field'),
           array('!id' => $iid, '!field' => $field));

  // Give other modules a chance to act after adding an index.
  $index = array('iid' => $iid,
                 'field' => $field,
                 'fts_table' => $fts_table,
                 'columns' => $columns,
                 'instances' => $instances);
  module_invoke_all('postgresql_fts_add_index_after', $index);

  return;
}

/**
 * Return chunks of data that are not yet indexed.
 * Let the database to the sifting, not PHP.
 *
 * We base filtering on entity_id, entity_type, deleted and language.
 * We ignore delta for the moment as we are merely interesting in getting
 * the entity_id and entity_type back to load the full field data
 * during a batch operation.
 *
 * @param $iid (integer)
 *   The ID of the index for which to check.
 * @param $offset (integer)
 *   The OFFSET from which to start.
 *   Note that this might prove a performance bottleneck on huge data sets.
 */
function _postgresql_fts_index_get_remaining_items($iid, $offset, $limit) {
  // Get the full index info.
  $index = _postgresql_fts_index_get_info($iid);
  $table = _postgresql_fts_field_get_table_name($index['field']);

  // Figure out which entity types and bundles to select.
  foreach($index['instances'] as $instance){
    $bundles[] = $instance['bundle'];
    $entity_types[] = $instance['entity_type'];
  }

  // Build the sifting query.
  $q = 'SELECT entity_id, entity_type, deleted, language '.
    'FROM {'.$table.'} f '.
    'WHERE NOT EXISTS ('.
      'SELECT entity_id, entity_type, deleted, language '.
      'FROM {'.$index['fts_table'].'} p '.
      'WHERE f.entity_id = p.entity_id '.
      'AND f.entity_type = p.entity_type '.
      'AND f.deleted = p.deleted '.
      'AND f.language = p.language'.
    ') '.
    'AND f.bundle IN (:bundles) '.
    'AND f.entity_type IN (:entity_types) '.
    'ORDER BY f.entity_id DESC '.
    'OFFSET :offset '.
    'LIMIT :limit';

  $query = db_query($q, array(':bundles' => $bundles,
                              ':entity_types' => $entity_types,
                              ':offset' => $offset,
                              ':limit' => $limit))
    ->fetchAll(PDO::FETCH_ASSOC);

  return $query;
}

/**
 * Count the total items that are not yet indexed.
 * Let the database to the sifting, not PHP.
 *
 * This function uses the same premise as _index_get_remaining_items().
 *
 * @param $iid (integer)
 *   The ID of the index for which to check.
 */
function _postgresql_fts_index_count_remaining_items($iid) {
  // Get the full index info.
  $index = _postgresql_fts_index_get_info($iid);
  $table = _postgresql_fts_field_get_table_name($index['field']);

  // Figure out which entity types and bundles to select.
  foreach($index['instances'] as $instance){
    $bundles[] = $instance['bundle'];
    $entity_types[] = $instance['entity_type'];
  }

  // Build the sifting query.
  $q = 'WITH count AS '.
       '( '.
         'SELECT entity_id, entity_type, deleted, language '.
         'FROM  {'.$table.'} '.
         'WHERE bundle IN (:bundles) '.
         'AND entity_type IN (:entity_types) '.
         'EXCEPT '.
         'SELECT entity_id, entity_type, deleted, language '.
         'FROM {'.$index['fts_table'].'} '.
         'ORDER BY entity_id DESC '.
        ') '.
       'SELECT count(entity_id) FROM count';

  $query = db_query($q, array(':bundles' => $bundles,
                              ':entity_types' => $entity_types))
    ->fetch();

  return $query->count;
}

/**
 * Given an Index ID, return the size on disk of its
 * database table (FTS table).
 *
 * @param $iid (integer)
 *   The Index ID for which to fetch the table size.
 *
 * @return
 *   An array containing the size converted to
 *   the right unit and the unit itself.
 */
function _postgresql_fts_index_get_size($iid) {
  // Get the FTS table name.
  $index_info = _postgresql_fts_index_get_info($iid);
  $table = $index_info['fts_table'];
  // Assume the default Schema for now.
  $schema = 'public';
  // Ask PostgreSQL about the raw bytes of the full table size
  // including indexes.
  // Have to resort to string stiching as we need to get the
  // possible table prefix in use for the current connection.
  $q = "SELECT pg_total_relation_size('".$schema.".{".$table."}') AS size";
  $query = db_query($q)
    ->fetch(PDO::FETCH_ASSOC);

  return _postgresql_fts_render_size($query['size']);
}

/**
 * Check if given field machine names are present in one or more indexes.
 *
 * @param $fields (array)
 *   The field machine names to check.
 *
 * @return
 *   Either FALSE (boolean) if no field is found or an array
 *   containing the various index IDs and their corresponding
 *   field machine names.
 */
function _postgresql_fts_field_fetch_iid($fields) {
  $q = 'SELECT iid, field '.
       'FROM {postgresql_fts} '.
       'WHERE field IN (:fields)';
  $query = db_query($q, array(':fields' => $fields))
    ->fetchAll(PDO::FETCH_ASSOC);

  if(!$query){
    return FALSE;
  }

  return $query;
}

/**
 * Fetch all fields in the Drupal installation.
 *
 * Field types fill be put into their own category.
 * Non-textual fields will be marked with an * to indicate
 * that they are not well suited for FTS indexation.
 *
 * @return
 *   An array of fields, keyed by type.
 */
function _postgresql_fts_field_get_all_fields() {
  $fields = array();

  $instances = field_info_field_map();

  foreach($instances as $name => $field) {
    switch($field['type']) {
      case 'text':
      case 'text_long':
      case 'text_with_summary':
      case 'list_text':
        $fields['Textual'][$name] = $name;
        break;
      case 'number_decimal':
      case 'number_float':
      case 'number_integer':
      case 'list_float':
      case 'list_integer':
        $fields['Number'][$name] = $name;
        break;
      default:
        $fields['Other'][$name] = $name;
    }
  }

  return $fields;
}

/**
 * Get all the columns that belong to a field.
 *
 * @param $field (string)
 *   The field machine name to fetch the columns for.
 *
 * @return
 *  An array containing the field columns.
 */
function _postgresql_fts_field_get_columns($field) {
  // Get all the info from the field
  $field_info = field_info_field($field);

  $columns = array();
  foreach($field_info['columns'] as $key => $column) {
    $columns[$key] = $key;
  }

  return $columns;
}

/**
 * Get all the instances of a field.
 *
 * @param $field (string)
 *   The machine name of the field for which to load the instances.
 *
 * @return
 *   An array of the different instances.
 */
function _postgresql_fts_field_get_instances($field) {
  // Get information about where the field is used.
  $instances = array();

  foreach (field_read_instances(array('field_name' => $field)) as $instance) {
    $instances[] = array('entity_type' =>$instance['entity_type'],
                         'bundle' => $instance['bundle'],
                         'label' => $instance['label']);
  }

  return $instances;
}

/**
 * Get the name of the field's storage table.
 *
 * @param (string)
 *   The field machine name.
 *
 * @return
 *   A string containing the name of the table in the database.
 */
function _postgresql_fts_field_get_table_name($field) {
  $field_info = field_info_field($field);
  $table = array_keys($field_info['storage']['details']['sql']['FIELD_LOAD_CURRENT']);

  return $table[0];
}

/**
 * Get the amount of rows in the actual field table
 * according to the instances set in the index.
 *
 * @param $field (string)
 *   The field for which to count rows.
 * @param $iid (integer)
 *   The ID of the index for which filtering out
 *   possible instances which should not be counted.
 *
 * @return
 *   The number of rows in the field table
 */
function _postgresql_fts_field_count_rows($field, $iid) {
  $info = _postgresql_fts_index_get_info($iid);
  $instances = $info['instances'];
  $table = _postgresql_fts_field_get_table_name($field);

  $bundles = array();
  $entity_types = array();
  foreach ($instances as $instance) {
    $bundles[] = $instance['bundle'];
    $entity_types[] = $instance['entity_type'];
  }

  $q = 'SELECT count(entity_id) '.
       'FROM {'.$table.'} '.
       'WHERE bundle IN (:bundles) '.
       'AND entity_type IN (:entity_types)';
  $query = db_query($q, array(':bundles' => $bundles,
                              ':entity_types' => $entity_types))
    ->fetch(PDO::FETCH_ASSOC);

  return $query['count'];
}