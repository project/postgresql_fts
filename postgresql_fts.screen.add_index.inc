<?php

/**
 * @file
 *
 * Contains the screen for adding a new index.
 * This includes the actual forms, the validator
 * and the submit handler.
 */

require_once('postgresql_fts.helpers.inc');
require_once('postgresql_fts.helpers.render.inc');

/**
 * Render the settings form to add a FTS index
 * and configure which items present on the field
 * should be indexed.
 */
function postgresql_fts_add_index($form, &$form_state) {
  // Build a small introduction text.
  $form['introduction'] = array(
    '#type' => 'markup',
    '#markup' => '<h2>Add a FTS index</h2>',
  );

  // Add a bogus "Select a field" option to the mix.
  $options = array(t('- select a field -'));
  $options = array_merge($options, _postgresql_fts_field_get_all_fields());

  $description = t('Select which field you wish to use for this index.');
  $description .= '</br>'.t('Note that <b>textual</b> fields are most suitable for FTS indexing.');
  $description .= '</br>'.t('Data from non-textual fields will be converted to text before indexing.');

  // Build the "fields" dropdown.
  $form['fields'] = array(
    '#type' => 'select',
    '#title' => t('Available fields'),
    '#description' => $description,
    '#options' => $options,
    '#ajax' => array(
      'callback' => 'postgresql_fts_add_index_ajax',
      'wrapper' => 'index_settings_wrapper',
      'effect' => 'fade',
    ),
  );

  // Put in the AJAX placeholders for column(s) and instance(s).
  $form['columns'] = array(
      '#type' => 'markup',
      '#markup' => '<div id="index_settings_columns"></div>',
  );
  $form['instances'] = array(
      '#type' => 'markup',
      '#markup' => '<div id="index_settings_instances"></div>',
  );

  // Build the columns dropdown, preparing it for an AJAX reload.
  if (isset($form_state['values']['fields'])) {
    $field = $form_state['values']['fields'];

    // Render the column(s) of the chosen field.
    $form['columns'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Available column(s) for "!field"', array('!field' => $field)),
      '#description' => t('Select which column(s) you wish to make available for this index.'),
      '#options' => _postgresql_fts_field_get_columns($field),
      '#prefix' => '<div id="index_settings_columns">',
      '#suffix' => '</div>',
    );

    // Render the instance(s) of the chosen field.
    // Prepare a nice description for each checkbox.
    $field_instances = _postgresql_fts_field_get_instances($field);
    $instances = array();
    foreach ($field_instances as $instance) {
      $index = $instance['entity_type'].'|'.$instance['bundle'];
      $instances[$index] = t('Entity type <b>!bundle</b> (!entity) with field label <b>!label</b>',
                     array('!entity' => $instance['entity_type'],
                           '!bundle' => $instance['bundle'],
                           '!label' => $instance['label']));
    }

    $form['instances'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Available instance(s) for "!field"', array('!field' => $field)),
      '#description' => t('Select which instance(s) you wish to make available for this index.'),
      '#options' => $instances,
      '#prefix' => '<div id="index_settings_instances">',
      '#suffix' => '</div>',
    );
  }

  // Build the submit button.
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Add new FTS index'),
  );

  return $form;
}

/**
 * Ajax callback to refresh the Columns list once chosen a field.
 */
function postgresql_fts_add_index_ajax($form, &$form_state) {
  if($form_state['values']['fields'] === '0'){
    // Reset the markup if needed.
    $form['columns'] = array(
      '#type' => 'markup',
      '#markup' => '<div id="index_settings_columns"></div>',
    );
    $form['instances'] = array(
      '#type' => 'markup',
      '#markup' => '<div id="index_settings_instances"></div>',
    );
  }

  // Return two AJAX command to re-render the two pieces of markup.
  return array(
    '#type' => 'ajax',
    '#commands' => array(
      ajax_command_replace('#index_settings_columns', render($form['columns'])),
      ajax_command_replace('#index_settings_instances', render($form['instances']))
    )
  );
}

/**
 * Validation handler for the Add Admins Settings form.
 *
 * Validate if a field and columns were chosen.
 */
function postgresql_fts_add_index_validate($form, &$form_state) {
  // If no fields selected, throw an error and stop validation.
  if($form_state['values']['fields'] === '0'){
    form_set_error('fields', t('Please select a field.'));
    return;
  }

  // Only validate when columns are present (not present in initial AJAX call).
  if(isset($form_state['values']['columns'])){
    // If fields selected but no columns, throw an error.
    $columns = array_filter($form_state['values']['columns']);
    if(empty($columns)){
      // @TODO: Make the columns select list color red...
      form_set_error('columns', t('Please select which column(s) you would like to index'));
    }
  }

  // Only validate when columns are present (not present in initial AJAX call).
  if(isset($form_state['values']['instances'])){
    // If fields selected but no columns, throw an error.
    $instances = array_filter($form_state['values']['instances']);
    if(empty($instances)){
      // @TODO: Make the columns select list color red...
      form_set_error('instances', t('Please select which instance(s) you would like to index'));
    }
  }
}

/**
 * Submit handler for the Add Admins Settings form.
 *
 * This will create the index table for the selected field and
 * update this module's bookkeeping table.
 *
 */
function postgresql_fts_add_index_submit($form, &$form_state) {
  // Create the custom table name.
  // Opted to base the table name of a random hash rather then the field name
  // as many index variations of the same field could be created.
  $fts_table = uniqid('pgfts_');

  // Fetch the field's storage table.
  $table = _postgresql_fts_field_get_table_name($form_state['values']['fields']);

  // Fetch the correct column names which are selected on submit
  // from the field's table.
  $field = field_info_field($form_state['values']['fields']);
  $columns = array();
  foreach ($form_state['values']['columns'] as $key => $column) {
    if($column !== 0) {
      $columns[$key] = $field['storage']['details']['sql']['FIELD_LOAD_CURRENT'][$table][$key];
    }
  }

  // Fetch the correct instance names.
  $instances = array();
  foreach ($form_state['values']['instances'] as $key => $instance) {
    if($instance !== 0) {
      // Extract the entity type and bundle from the key
      list($entity_type, $bundle) = explode('|', $key);
      $instances[] = array('entity_type' => $entity_type,
                           'bundle' => $bundle);
    }
  }

  // Check if the combination of field, columns or instances
  // already exists and if so, tell the user about it.
  if (_postgresql_fts_index_check_combination($form_state['values']['fields'], $columns, $instances)){
    drupal_set_message(t('There already seems to be an index for <b>!field</b> with the following columns: <b>!columns</b> and instances: <b>!instances</b>.',
        array('!field' => $form_state['values']['fields'],
              '!columns' => implode(', ', $columns),
              '!instances' => implode(', ', _postgresql_fts_render_instance_list_error($instances)))),
        'warning');

    return;
  }

  // Create the custom FTS table for this index.
  $transaction = db_transaction();
  try {
    // Create the empty FTS table
    $q = 'CREATE TABLE {'.$fts_table.'} ()';
    $query = db_query($q);

    // Setup the default primary key columns
    // for field data tables.
    $primary_keys = array('entity_type' => 'varchar',
                          'entity_id' => 'bigint',
                          'deleted' => 'smallint',
                          'delta' => 'bigint',
                          'language' => 'varchar');

    // Add the needed columns that correspond to the
    // field's table primary key columns.
    foreach ($primary_keys as $key => $type) {
      $q = 'ALTER TABLE {'. $fts_table .'} '.
        'ADD '. $key .' '.$type;
      $query = db_query($q);
    }

    // Add the foreign constraint to our field table
    // using the retrieved field's table primary key column(s).
    $q = "ALTER TABLE {". $fts_table ."} ".
      "ADD CONSTRAINT original_field_reference ".
      "FOREIGN KEY (". implode (", ", array_keys($primary_keys)) .") ".
      "REFERENCES {". $table ."} (". implode (", ", array_keys($primary_keys)) .") ".
      "ON DELETE CASCADE";
    $query = db_query($q);

    // Add each desired column to the custom FTS table.
    foreach ($columns as $column) {
      $q = 'ALTER TABLE {'. $fts_table .'} '.
        'ADD '. $column .' TSVECTOR';
      $query = db_query($q);
    }
  }
  catch (Exception $e) {
    // Something not right? Rollback everything and log.
    $transaction->rollback();
    watchdog_exception('PostgreSQL FTS Exception', $e);
  }

  // Update our bookkeeping and redirect to the main dashboard.
  _postgresql_fts_index_add_index($form_state['values']['fields'],
                                  $fts_table,
                                  $columns,
                                  $instances);

  // Let the user know everything went according to plane and redirect.
  drupal_set_message(t('New FTS index successfully added.'));
  $form_state['redirect'] = 'admin/config/search/postgresql-fts';
}
